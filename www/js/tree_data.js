var treeData = [{
    "name": "Kim Hjong-čik",
    "sex": "man",
    "children": [{
        "name": "Kang Pan-suk",
        "sex": "woman",
        "children": [{
            "name": "Kim Jong-ču",
            "sex": "man"
        }, {
            "name": "Kim Ir-sen",
            "sex": "man",
            "children": [{
                    "name": "Kim Čong-suk",
                    "sex": "woman",
                    "children": [{
                            "name": "KIM ČONG-IL",
                            "sex": "man",
                            "important": true,
                            "children": [{
                                    "name": "Hong Il-čchon",
                                    "sex": "woman",
                                    "children": [{
                                        "name": "Kim Hje-k jong",
                                        "sex": "woman",
                                    }]
                                },
                                {
                                    "name": "Song Hje-rim",
                                    "sex": "woman",
                                    "children": [{
                                            "name": "Kim Čong-nam",
                                            "sex": "man",
                                            "children": [{
                                                "name": "Hje-kjong",
                                                "sex": "woman",
                                                "children": [{
                                                        "name": "Kim Sol-hui",
                                                        "sex": "woman",
                                                    },
                                                    {
                                                        "name": "Kim Han-sol",
                                                        "sex": "man",
                                                    }
                                                ]
                                            }]
                                        }

                                    ]
                                },
                                {
                                    "name": "Kim Jong-suk",
                                    "sex": "woman",
                                    "children": [{
                                            "name": "Kim Sol-song",
                                            "sex": "dmnt",
                                        }

                                    ]
                                },
                                {
                                    "name": "Ko Jong-hui",
                                    "sex": "woman",
                                    "children": [{
                                            "name": "Kim Čong-čchol",
                                            "sex": "man",
                                            "children": [{
                                                    "name": "Neznámá",
                                                    "sex": "woman",
                                                    "children": [{
                                                        "name": "?",
                                                        "sex": "",
                                                    }]
                                                }

                                            ]
                                        },
                                        {
                                            "name": "KIM ČONG-UN",
                                            "sex": "man",
                                            "children": [{
                                                    "name": "Ri Sol-ču",
                                                    "sex": "woman",
                                                    "children": [{
                                                            "name": "?",
                                                            "sex": "",
                                                        },
                                                        {
                                                            "name": "Kim Ču-e",
                                                            "sex": "woman",
                                                        }

                                                    ]
                                                }

                                            ]
                                        },
                                        {
                                            "name": "Kim Jo-čon",
                                            "sex": "woman",
                                        }
                                    ]
                                },
                                {
                                    "name": "Kim Ok",
                                    "sex": "woman",
                                }
                            ]
                        },
                        {
                            "name": "Kim Man-il",
                            "sex": "man",
                        },
                        {
                            "name": "Kim Kjong-hui",
                            "sex": "woman",
                            "children": [{
                                "name": "Čang Song-tche",
                                "sex": "man"
                            }]
                        }
                    ]
                },
                {
                    "name": "Kim Song-e",
                    "sex": "woman",
                    "children": [{
                            "name": "Kim Pchjong-il",
                            "sex": "man",
                        },
                        {
                            "name": "Kim Jong-il",
                            "sex": "man"
                        },
                        {
                            "name": "Kim Kjong-čin",
                            "sex": "woman",
                            "children": [{
                                "name": "Kim Kwang-sop",
                                "sex": "man",
                            }]
                        }
                    ]
                },
                {
                    "name": "Ošetřovatelka",
                    "sex": "woman",
                    "children": [{
                        "name": "Kim Hjun",
                        "sex": "man",
                    }]
                }
            ]
        }]
    }]
}];